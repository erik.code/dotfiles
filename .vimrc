" MAKE ALT-KEY MAPPABLE
" ensure: set convert-meta on

let c='a'
while c <= 'z'
  exec "set <A-".c.">=\e".c
  exec "imap \e".c." <A-".c.">"
  let c = nr2char(1+char2nr(c))
endw
set timeout ttimeoutlen=50

" COLORS
syntax on
highlight LineNr ctermfg=grey

" GUI SETTINGS
set number
set clipboard=unnamedplus

set autoindent
set expandtab
set shiftwidth=4
set tabstop=4

set hlsearch
set ignorecase
set incsearch
set smartcase

set encoding=utf-8

set laststatus=2
set ruler
set wildmenu
set cursorline
set relativenumber


" MOVE LINE WITH ALT
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi

" MOVE SELECTED TEXT WITH ALT
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv

" MOVE TO NEXT BRACKET
map <C-k> [m
map <C-j> ]m
