# PRIVATE ALIASES
source ~/.bash_aliases_private

# ADD GIT BRANCH TO TERMINAL PROMPT 
parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

export PS1="\u@\h \[\033[32m\]\w\[\033[33m\]\$(parse_git_branch)\[\033[00m\] $ "

set convert-meta on

# COPY PUBLIC KEY TO SERVER
copy-ssh(){
	hostip=$1
	username=$2
	cat ~/.ssh/id_rsa.pub | ssh $username@$hostip "mkdir -p ~/.ssh && touch ~/.ssh/authorized_keys && chmod -R go= ~/.ssh && cat >> ~/.ssh/authorized_keys"
}
